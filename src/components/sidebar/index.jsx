import React, {Component} from "react";
import {Input, Button, Modal, ModalHeader, ModalBody} from "reactstrap";
import PollList from "./poll-list";

class SideBar extends Component {

    state = {
        openModal: false
    };

    toggleSelect = () => {
        this.setState({
            openModal: !this.state.openModal
        })
    }

    render() {
        return (
            <div style={{background: "#efefef", padding: "10px"}}>
                <div className="d-flex mb-5">
                    <Input type="search" placeholder="Enter Your Keyword" value={this.props.searchTerm}
                           onChange={(e) => this.props.handleSearch(e.target.value)}/>

                    <Button color="success" className="ml-2" onClick={this.toggleSelect}>New</Button>
                </div>
                    <h3>List of Polls</h3>
                    <hr/>
                    <PollList selectPoll={this.props.selectPoll} polls={this.props.polls}/>
                    <Modal isOpen={this.state.openModal} toggle={this.toggleSelect} unmountOnClose={true}>
                        <ModalHeader toggle={this.toggleSelect}>
                            Create a New poll
                        </ModalHeader>
                        <ModalBody>
                            This is a Poll Modal body
                        </ModalBody>
                    </Modal>
            </div>
        )
    }
}

export default SideBar