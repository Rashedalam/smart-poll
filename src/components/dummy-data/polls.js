const polls = [
    {
        id: "1",
        title: "This is Poll One",
        description: "This is Poll Description One",
        options: [
            {
                id: 1, value: "C Programming", vote: 0
            },
            {
                id: 1, value: "Java", vote: 0
            },
            {
                id: 1, value: "Javascript", vote: 0
            },
            {
                id: 1, value: "Paython", vote: 0
            }
        ],
        created: new Date(),
        totalVote: 0,
        opinions: []
    },
    {
        id: "2",
        title: "This is Poll Two",
        description: "This is Poll Description Two",
        options: [
            {
                id: 1, value: "C Programming", vote: 0
            },
            {
                id: 1, value: "Java", vote: 0
            },
            {
                id: 1, value: "Javascript", vote: 0
            },
            {
                id: 1, value: "Paython", vote: 0
            }
        ],
        created: new Date(),
        totalVote: 0,
        opinions: []
    }
];

export default polls