import React, {Component} from "react";
import {Container, Row, Col} from "reactstrap";
import SideBar from "./components/sidebar";
import MainContent from "./components/main-content";
import POLLS from "./components/dummy-data/polls.js"
import shortId from "shortid";

class App extends Component {

    state = {
        polls: [],
        selectedPoll: {},
        searchTerm: ""
    };

    componentDidMount() {
        this.setState({polls: POLLS})
    }

    addNewPoll = (poll) => {
        poll.id = shortId.generate()
        poll.created = new Date()
        poll.totalVote = 0;
        poll.opinions = [];

        this.setState({
            polls: [poll, ...this.state.polls]
        })

    };

    updatePoll = (updatePoll) => {
        const polls = [...this.state.polls];
        const poll = polls.find(p => p.id === updatePoll.id);
        poll.title = updatePoll.title;
        poll.description = updatePoll.description;
        poll.opinions = updatePoll.opinions;

        this.setState({polls})

    };

    deletePoll = (id) => {
        const polls = this.state.polls.filter(p => p.id !== id);

        this.setState({polls})
    };

    selectPoll = (id) => {
        const poll = this.state.polls.find(p => p.id === id);
        this.setState({
            selectedPoll: poll
        })
    };

    handleSearch = (e) => {

    };

    render() {
        return (
            <Container className='my-4'>
                <Row>
                    <Col md={4}>
                        <SideBar searchTerm={this.state.searchTerm} handleSearch={this.handleSearch}
                                 selectPoll={this.selectPoll} polls={this.state.polls}/>
                    </Col>
                    <Col md={8}>
                        <MainContent/>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default App
